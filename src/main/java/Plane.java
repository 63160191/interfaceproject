/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author User
 */
public class Plane extends Vahicle implements Flyable,Runable{
    private String name;

    public Plane(String name){
        super("Plane");
        this.name=name;
    }
    @Override
    public void startEngine() {
        System.out.println("Plane: "+name+" Start");
    }

    @Override
    public void stopEngine() {
        System.out.println("Plane: "+name+" Stop");
    }

    @Override
    public void raiseSpeed() {
        System.out.println("Plane: "+name+" Speed");
    }

    @Override
    public void applyBreak() {
        System.out.println("Plane: "+name+" Break");
    }
    @Override
    public void fly() {
        System.out.println("Plane: "+name+" fly");
    }

    @Override
    public void run() {
        System.out.println("Plane: "+name+" run");
    }
}
