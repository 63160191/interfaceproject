/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author User
 */
public class Bat extends Poulty{
    private String name;
    
    public Bat(String name) {
        super("Bat", 2);
        this.name=name;
    }
    @Override
    public void eat() {
        System.out.println("Bat: " + name + " eat");
    }

    @Override
    public void speak() {
        System.out.println("Bat: " + name + " speak");
    }

    @Override
    public void sleep() {
        System.out.println("Bat: " + name + " sleep");
    }

    @Override
    public void fly() {
        System.out.println("Bat: " + name + " fly");
    }
    
}
