/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author User
 */
public class TestFlyable {
    public static void main(String[] args){
        Bat bat = new Bat("Mamo");
        Plane plane = new Plane("Engine number 1");
        bat.fly();
        plane.fly();
        Dog dog = new Dog("Tang");
        
        Flyable[] flyable ={bat,plane};
        for(Flyable f:flyable){
            if(f instanceof Plane){
                Plane p = (Plane)f;
                p.startEngine();
                p.run();
            }
            f.fly();
        }
        
        Runable[] runable ={dog,plane};
        for(Runable r:runable){
            if(r instanceof Plane){
                Plane p = (Plane)r;
                p.startEngine();
            }
            r.run();
        }
    }
}
